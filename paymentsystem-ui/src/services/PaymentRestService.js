import http from "../http-common";

// entry point  to all the rest apis

export class PaymentService {
    getAll() {
        return http.get("/");
    }

    getStatus() {
        return http.get("/all");
    }

    getID(id) {
        return http.get(`/id/${id}`);
    }

    getType(type) {
        return http.get(`/type/${type}`);
    }

    create(data) {
        return http.post("/", data);
    }
}

export default new PaymentService();