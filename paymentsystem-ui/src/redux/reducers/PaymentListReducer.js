import {PAYMENT_LIST_REQUEST , PAYMENT_LIST_SUCCESS , PAYMENT_ADD_REQUEST , PAYMENT_ADD_SUCCESS ,PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"

//Payload is what is bundled in your actions and passed around between reducers in your redux application.
// //For example - const someAction = { type: "Test", payload: {user: "Test User", age: 25}, }
const paymentsInitialState = {
    pending: false,
    payments: [],
    error: null
}

// export function PaymentListReducer(state=paymentsInitialState, action)
//  function PaymentListReducer(state=paymentsInitialState, action)
function PaymentReducer(state=paymentsInitialState, action)
{
    console.log("Reducer Type = " + action.type);
    switch (action.type) {
        // initial req will set pending to true below
        case PAYMENT_LIST_REQUEST:
            console.log(">>>Reducer 1");
            return { ...state, pending: true };
        case PAYMENT_LIST_SUCCESS:
            console.log(">>> reducer 2");
            //action.payload is the result of the request
            return { ...state, pending: false, payments: action.payload };
        case PAYMENT_LIST_FAIL:
            console.log(">>> reducer 3");
            return { ...state, pending: false, error: action.payload };

        case PAYMENT_ADD_REQUEST:
            return { ...state, customer: action.payload };
        case PAYMENT_ADD_SUCCESS:
            return { ...state, submitted: true, payment: action.payload };

        default:
            return state;
    }
}
 // export default  PaymentListReducer;
export default  PaymentReducer;