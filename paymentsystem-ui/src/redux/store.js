import reducer from "./reducers";

import {applyMiddleware, combineReducers, createStore} from "redux";
// import  PaymentListReducer from "./reducers/PaymentListReducer"
import  PaymentReducer from "./reducers/PaymentListReducer"
import thunk from "redux-thunk";
const initialState = {};
export const middlewares = [thunk];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;

export const store = createStore(
    PaymentReducer, //PaymentListReducer, //ListReducer, //typically combine all reducers
    initialState,
    composeEnhancer(applyMiddleware(...middlewares))
);


// this will be store settings
// import {applyMiddleware, combineReducers, createStore} from "redux";
// import  combineReducers from "./reducers/CombineReducer"
// import thunk from "redux-thunk";
// //import PaymentAddReducer from "./reducers/PaymentAddReducer";
// const initialState = {};
// export const middlewares = [thunk]; // this is only one of middleware solutions  that could be use
// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;
//
// export const store = createStore(
//     rootReducer, //PaymentListReducer, PaymentAddReducer, //typically combine all reducers
//     initialState,
//     //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
//     composeEnhancer(applyMiddleware(...middlewares))
// );

//export default store;