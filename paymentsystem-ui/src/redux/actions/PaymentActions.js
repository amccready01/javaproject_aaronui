//Actions are the only source of information for the store
// actions are plain JavaScript object that must have a type attribute to indicate the type of action performed. It tells us what had happened. Types should be defined as string constants in your application as given below −
//
// const ITEMS_REQUEST = 'ITEMS_REQUEST';
// Apart from this type attribute, the structure of an action object is totally up to the developer.
// It is recommended to keep your action object as light as possible
// and pass only the necessary information.
//
//     To cause any change in the store, you need to dispatch an action first by using store.dispatch() function. The action object is as follows −
//
// { type: GET_ORDER_STATUS , payload: {orderId,userId } }
// { type: GET_WISHLIST_ITEMS, payload: userId }

import {EMPLOYEE_LIST_REQUEST , EMPLOYEE_LIST_SUCCESS , EMPLOYEE_LIST_FAIL}
    from "../constants/EmployeeConstants"


import EmployeeService from "../../services/EmployeeRestService";
const paymentList = () =>  (dispatch) => {
    try {
        console.log(">>> in dispatch ");
        dispatch({ type: EMPLOYEE_LIST_REQUEST });
        const data =  PaymentService.getAll(); //axios.get(CUSTOMERS_URI);
        console.log(">>> in dispatch CUSTOMERS_URI= + got data");
        dispatch({ type: EMPLOYEE_LIST_SUCCESS, payload: data });
    } catch (error) {
        console.log(">>> in dispatch error=" + error);
        dispatch({ type: EMPLOYEE_LIST_FAIL, payload: error });
    }
};

//
// export default  PaymentList;
//   export function fetchEMPLOYEESPending() {
//     return {
//         type: EMPLOYEE_LIST_REQUEST
//     }
// }
//
//   export function fetchEMPLOYEESSuccess(payments) {
//     return {
//         type: EMPLOYEE_LIST_SUCCESS,
//         payments: payments
//     }
// }
//
//    export function fetchEMPLOYEESError(error) {
//     return {
//         type: EMPLOYEE_LIST_FAIL,
//         error: error
//     }
//}

export default  paymentList;