import React, {Component} from "react";
import PaymentService from "../services/PaymentRestService";
import {Redirect} from "react-router-dom";




// function AddPayments(props) {
export default class  AddPayments extends  Component {
    constructor(props) {
        super(props);
        this.onChangeFname = this.onChangeFname.bind(this);
        this.onChangeLname = this.onChangeLname.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeSalary = this.onChangeSalary.bind(this);
        this.onChangeId = this.onChangeId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            paymentid: 0,
            paymentdate: "",
            type: "",
            amount: 0,
            custid: "",
            submitted: false
        };
    }

    onChangePaymentid(e) {
        this.setState({
            id: e.target.value
        });
    }

    onChangepaymentdate(e) {
        this.setState({
            paymentdate: e.target.value
        });
    }

    onChangetype(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangeamount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    onChangecustid(e) {
        this.setState({
            custid: e.target.value
        });
    }

    savePayment() {
        var data = {
            paymentid: this.state.paymentid,
            paymentdate: this.state.paymentdate,
            type: this.state.type,
            amount: this.state.amount,
            custid: this.state.custid
      }

        PaymentService.create(data)
            .then(response => {
                this.setState({
                    // id: response.data.id,
                    // fname: response.data.fname,
                    // lname: response.data.lname,
                    // email: response.data.email,
                    // salary: response.data.salary,
                    submitted: true
                });

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }
    render() {
        // so if Payment added (submitted) return to home page
        if (this.state.submitted) {
            return <Redirect to="home" />
        }
        return (
            <form>
                <h1>Add Payments</h1>
                <div className="form-group">
                    <label htmlFor="paymentID">Payment ID</label>
                    <input type="text" className="form-control" id="paymentID" aria-describedby="paymentIDHelp"
                           placeholder="Enter id" value={this.state.fname} onChange={this.onchange}/>
                    <small id="paymentIDHelp" className="form-text text-muted">We'll never share your details with anyo
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="paymentDate">Type</label>
                    <input type="date" className="form-control" id="paymentDate" aria-describedby="paymentDateHelp"
                           placeholder="Enter payment date" value={this.state.paymentDate} onChange={this.onChangePaymentDate}/>
                    <small id="paymentDateHelp" className="form-text text-muted">We'll never share your details with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="paymentAmount">Amount</label>
                    <input type="number" className="form-control" id="paymentAmount" aria-describedby="paymentAmountHelp"
                           placeholder="Enter Payment Amount" value={this.state.email} onChange={this.onChangeEmail}/>
                    <small id="paymentAmountHelp" className="form-text text-muted">We'll never share your email with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleSalary">Customer ID</label>
                    <input type="number" className="form-control" id="paymentCustomerID" aria-describedby="paymentCustomerIDHelp"
                           placeholder="Enter salary" value={this.state.salary} onChange={this.onChangeSalary}/>
                    <small id="paymentCustomerID" className="form-text text-muted">We'll never share your salary with anyone
                        else.</small>
                </div>

                <div className="form-group">

                    <input type="button" className="form-control" value={"save"} onClick={this.savePayment}/>
                </div>

            </form>
        );
    }
}

