import React, {Component} from 'react';
import PaymentService from "../services/PaymentRestService";
import paymentList from "../redux/actions/PaymentListActions";
import {connect} from "react-redux";
import { Link } from 'react-router-dom';


//export default class  ListPaymentsRedux extends  Component {
class  ListPaymentsRedux extends Component {
    constructor(props) {
        super(props);
        console.log("ListEmpRedux before call " + props)
        //this below goes to action
        this.props.dispatch(paymentList())
        console.log("ListEmpRedux after call " + props)
    }
    render() {
        // const {  Payments,currentIndex} = this.state;
        //as redux we use get data from props rather than state
        const {  payments, pending, error} = this.props;
        console.log("ListEmpRedux render() " + this.props)
        console.log("payments - " + payments)
        return  (
            <div className="col-md-6">
                <h4>Payments List</h4>

                {/* li's won't be created if Payments is empty/null */}
                <div className="">Click link for detail view</div>
                {/*<div className="">Click on link for further details</div>*/}
                <ul className="list-group list-group-mine ">
                    {payments &&
                    payments.map((payment, index) => (

                        <li
                            // className={'list-group-item ' + (index === 0 ? 'active' : '')}
                            className={'list-group-item ' }
                            // onClick={() => this.setActiveTutorial(tutorial, index)}
                            key={index}
                        >
                            <div className="row">
                                <div className="col-4">
                                    <Link to={`payment/${payment.id}`}>
                                        {payment.id}
                                    </Link>
                                </div>
                                <div className="col-4">
                                    {payment.amount}
                                </div>
                                {/*<div className="col-4">col-4</div>*/}
                            </div>
                             {/*{payment.paymentdate} {payment.type} {payment.amount} {payment.custid}*/}
                            {/*{payment.type} {payment.amount} {payment.custid}*/}
                        </li>
                    ))}
                </ul>

            </div>
        )
    }

}

//calls PaymentsList action

// this creates prop called dispatch
const mapDispatchToProps = (dispatch) => {
    return {
        //this calls constructor above
        dispatch: () => dispatch(paymentList())
    };
};

//maps state into props - is run automatically when state changes
const mapStateToProps = state => ({
    error:state.error,
    payments: state.payments,
    pending: state.pending
})

export default connect(mapStateToProps,mapDispatchToProps)(ListPaymentsRedux);


// function ListPayments(props) {
//     return (<div>
//             <h1>Payment List</h1>
//             <h1>{props.appname}</h1>
//             </div>)
//
// }

// export default ListPayments;