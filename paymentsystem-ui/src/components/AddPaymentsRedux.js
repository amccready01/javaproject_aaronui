import React, { useState } from "react";
import PropTypes from "prop-types";
import paymentAdd from "../redux/actions/PaymentAddActions";
import {useDispatch} from "react-redux";

function AddPaymentsRedux(props) {
    const dispatch = useDispatch();
    const initialFormState = {
        id:"",
        paymentdate: "",
        type:"",
        amount:0,
        custid:"",
        submitted: false };
    const [payment, setPayment] = useState(initialFormState);
    const handleInputChange = (event) => {
        const { name, value } = event.target;

        setPayment({ ...payment, [name]: value });
    };


    return (<form
            onSubmit={(event) => {
                event.preventDefault();
                dispatch(paymentAdd(payment));
                // const onNewPaymentSubmitHandler = (payment) => {
                //     dispatch(paymentAdd(payment));
                // };

                // props.onNewPaymentSubmitHandler(payment);
            }}>
            <h1>Add Payments</h1>
            <div className="form-group">
                <label htmlFor="paymentID">Payment ID</label>
                <input type="text" className="form-control" id="paymentID"  name="id" aria-describedby="paymentIDHelp"
                       placeholder="Payment ID" value={payment.id}  required onChange={handleInputChange}/>
                <small id="paymentIDHelp" className="form-text text-muted">We'll never share your details with anyo
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="paymentDate">Payment Date</label>
                <input type="date" className="form-control" id="paymentDate" name="paymentdate" aria-describedby="paymentDateHelp"
                       placeholder="Enter date" value={payment.paymentdate}  required onChange={handleInputChange}/>
                <small id="paymentDateHelp" className="form-text text-muted">We'll never share your details with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="paymentType">Payment Type</label>
                <input type="text" className="form-control" id="paymentType" name="type" aria-describedby="paymentTypeHelp"
                       placeholder="Enter payment type" value={payment.type}  required onChange={handleInputChange}/>
                <small id="paymentTypeHelp" className="form-text text-muted">We'll never share your email with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="amount">Amount</label>
                <input type="number" className="form-control" id="amount"  name="amount" aria-describedby="amountHelp"
                       placeholder="Enter payment amount" value={payment.amount} required onChange={handleInputChange}/>
                <small id="amountHelp" className="form-text text-muted">We'll never share your salary with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="id">Customer ID</label>
                <input type="number" className="form-control" id="custid" name="custid" aria-describedby="custidHelp"
                       placeholder="Enter Customer id" value={payment.custid} onChange={handleInputChange}/>
                <small id="custidHelp" className="form-text text-muted">We'll never share your salary with anyone
                    else.</small>
            </div>
            <div className="form-group">

                <input type="submit" className="form-control" value="Save" ></input>
            </div>

        </form>
    );
}

// AddPaymentsRedux.propTypes = {
//     onSubmitHandler: PropTypes.func.isRequired,
// };

export default AddPaymentsRedux;
