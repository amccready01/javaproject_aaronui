import React, {Component} from "react";

export default class About extends Component
{
    constructor(props) {
        super(props);
        this.state = {name: "User1.", year: "2020", apistatus: "Unknown"}
    }

    render() {
        // here we can return the jsx
        return (
            <div>
                <h1>Payment System {this.state.name}</h1>
                <h2>Year is {this.state.year}</h2>
                <h3>Status is {this.state.apistatus}</h3>
            </div>
        )
    }

    async componentDidMount(){
        var url = "http://localhost:8080/api/payment/status"
        let textData
        try {
            let response = await fetch(url);
            textData = await response.text();
            console.log('The data is: ' + textData);
            //document.getElementById("selectedProduct").innerTest=textData;
        }
        catch(e) {
            console.log('The err is: ' + e.toString());
        }
        this.setState({apistatus:textData});
        return textData;
    }
}

// you can also export class here rather than do export default above
//export default About or class About